import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppFormsModule } from '../shared/forms/app-forms.module';
import { AddComponent } from './add/add.component';
import { ProductsRoutingModule } from './products-routing.module';
import { TestComponent } from './test/test.component';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { AddService } from './add/add.service';
import { ModalComponent } from './modal/modal.component';
import { DialogModule } from '../shared/dialog/dialog.module';

const testStrategy = () => {
  return (control: FormControl | null, form: FormGroupDirective | NgForm | null) => {
    return (control && control.invalid && (control.dirty || (form && form.submitted)));
  };
};

const COMPONENTS = [
  AddComponent,
  TestComponent,
  ModalComponent,
];

@NgModule({
  imports: [
    CommonModule,
    // AppFormsModule.setStrategies(testStrategy),
    AppFormsModule,
    DialogModule,
    ProductsRoutingModule,
  ],
  declarations: [
    ...COMPONENTS
  ],
  entryComponents: [
    ModalComponent
  ],
  providers: [
    AddService
  ],
  exports: []
})
export class ProductsModule {

}
