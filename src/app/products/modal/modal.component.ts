import { Component, Inject, OnInit } from '@angular/core';
import { DialogRef } from '../../shared/dialog/dialog-ref';
import { DIALOG_DATA } from '../../shared/dialog/dialog-injectors';
import { DialogService } from '../../shared/dialog/dialog.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  constructor(private dialogRef: DialogRef<ModalComponent>,
              @Inject(DIALOG_DATA) public data: any,
              private dialog: DialogService) {
  }

  public ngOnInit() {
    // this.dialogRef.disableClose = true;
    setTimeout(() => {
      this.dialogRef.close({test: '123'});
    }, 3000);
  }

  public open() {
    this.dialog.openFromComponent(ModalComponent);
  }
}
