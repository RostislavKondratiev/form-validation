import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { TestComponent } from './test/test.component';

const ROUTES: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'add'},
  {path: 'add', component: AddComponent},
  {path: 'test', component: TestComponent}
];

@NgModule({
  imports: [RouterModule.forChild(ROUTES)],
  exports: [RouterModule]
})
export class ProductsRoutingModule {

}
