import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { OptionModel } from '../../shared/forms/option/option.model';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';

@Injectable()
export class AddService {
  constructor(private http: HttpClient) {
  }

  public search(value: string) {
    let params = new HttpParams();
    params = params.append('q', value);
    return this.http.get<OptionModel[]>('http://localhost:3000/posts/', {params})
      .pipe(
        map((res) => res.map((item: any) => ({label: item.title, value: item.id})))
      );
  }

  public test(value: string) {
    return of(['Every', 'Where', 'I go'])
      .pipe(
        map((res) => res.map((item: any, index) => ({label: item, value: index})))
      );
  }
}
