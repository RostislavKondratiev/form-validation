import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import {
  ServerStrategyHandler,
  ServerValidationStrategy,
  ValidationStrategy
} from '../../shared/forms/errors/errors.strategies';
import { AppValidators } from '../../helpers/validators';
import { AddService } from './add.service';
import { TypeaheadService } from '../../shared/forms/typeahead/typeahead.service';
import { ModalComponent } from '../modal/modal.component';
import { DialogService } from '../../shared/dialog/dialog.service';
import { Overlay } from '@angular/cdk/overlay';

const testStrategy: ValidationStrategy = () => {
  return (control, form) => {
    return (control && control.invalid && (control.dirty || (form && form.submitted)));
  };
};

const nonFieldStrategy: ServerValidationStrategy = (): ServerStrategyHandler => {
  return (source: FormGroupDirective | NgForm) => {
    return !(source.invalid && source.submitted);
  };
};

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
  providers: [
    TypeaheadService,
    // {provide: validationStrategy, useFactory: testStrategy}
  ]
})
export class AddComponent implements OnInit {
  public addForm = new FormGroup({
    title: new FormControl(null, [Validators.required, Validators.minLength(5), Validators.maxLength(20)]),
    price: new FormControl(null, [Validators.required]),
    details: new FormControl(null, [Validators.maxLength(50)]),
    car: new FormControl(null, [Validators.required]),
    agree: new FormControl(null, [Validators.requiredTrue]),
    typeahead: new FormControl(null, [Validators.required]),
    testTypeadhead: new FormControl(null, [Validators.required]),
    type: new FormControl(null, [Validators.required]),
    contacts: new FormGroup({
      email: new FormControl(null, [Validators.required, AppValidators.email]),
      address: new FormGroup({
        city: new FormControl(null, [Validators.required]),
        street: new FormControl(null, [Validators.required, Validators.minLength(5)]),
        district: new FormControl(null, [Validators.required])
      })
    })
  });
  @ViewChild('searchTemplate') public searchTemplate: TemplateRef<any>;
  @ViewChild('modal') public modal: TemplateRef<any>;
  @ViewChild('anchor') public anchor: TemplateRef<any>;

  public testStrategy = testStrategy();
  public nonErrorFieldStrategy = nonFieldStrategy();
  public options = [{label: 'Center', value: 0}, {label: 'North', value: 1}, {label: 'South', value: 2}];
  public nonForm;
  public standaloneRadio = 1;
  public control = new FormControl();

  public serverError = null;
  public nonGroups = [
    {label: 'non group 1', value: 0},
    {label: 'non group 2', value: 222},
  ];
  public recursiveItems = [
    {
      label: 'group 1', group: [
        {label: 'first', value: 1},
        {label: 'second', value: 2}
      ]
    },
    {
      label: 'group 2', group: [
        {label: 'third', value: 3},
        {label: 'fourth', value: 4}
      ]
    }
  ];

  constructor(private service: AddService,
              private thService: TypeaheadService,
              private dialog: DialogService,
              private overlay: Overlay) {
    this.thService.addSearch({key: 'search', func: service.search.bind(service)});
    this.thService.addSearch({key: 'test', func: service.test.bind(service)});
  }

  public ngOnInit() {
    setTimeout(() => {
      // this.addForm.get('contacts').get('address').get('district').setValue(2);
    }, 1000);
    setTimeout(() => {
      // this.options = [{label: 'Center', value: 0}, {label: 'North', value: 1}, {label: 'South', value: 2}];
    }, 2000);
  }

  public openModal() {
    const dialog = this.dialog.openFromComponent(ModalComponent, {
      width: '60vw',
      data: {text: 'Modal Data text'},
      disableClose: true
    });
    dialog.afterClosed().subscribe((val) => {
      console.log('closed');
      console.log(val);
    });
  }

  public submit($event) {
    $event.preventDefault();
    this.serverError = {
      title: 'This title is already used',
      contacts: {
        email: 'invalid Email',
        address: {
          street: 'Street not found'
        },
      },
      non_field_error: 'Non Field Error'
    };
  }
}
