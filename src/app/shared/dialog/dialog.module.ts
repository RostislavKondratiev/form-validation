import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { A11yModule } from '@angular/cdk/a11y';
import { DialogConfig } from './dialog-config';
import { DialogRef } from './dialog-ref';
import { DIALOG_CONFIG, DIALOG_CONTAINER, DIALOG_REF, DIALOG_SCROLL_STRATEGY_PROVIDER } from './dialog-injectors';
import { DialogService } from './dialog.service';
import { DialogContainerComponent } from './dialog-container/dialog-container.component';


@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    PortalModule,
    A11yModule,
  ],
  exports: [
    DialogContainerComponent,
  ],
  declarations: [
    DialogContainerComponent,
  ],
  providers: [
    DialogService,
    DIALOG_SCROLL_STRATEGY_PROVIDER,
    {provide: DIALOG_REF, useValue: DialogRef},
    {provide: DIALOG_CONTAINER, useValue: DialogContainerComponent},
    {provide: DIALOG_CONFIG, useValue: DialogConfig},
  ],
  entryComponents: [DialogContainerComponent],
})
export class DialogModule {
}
