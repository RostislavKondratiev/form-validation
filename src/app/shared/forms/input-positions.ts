export enum InputPositions {
  Left = 'left',
  Right = 'right',
  LeftTransparent = 'left-transparent',
  RightTransparent = 'right-transparent'
}
