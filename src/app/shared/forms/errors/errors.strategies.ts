import { InjectionToken } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';

export type ValidationStrategy = () => StrategyHandler;
export type StrategyHandler = (control: FormControl | null, form: FormGroupDirective | NgForm | null) => boolean;

export type ServerValidationStrategy = () => ServerStrategyHandler;
export type ServerStrategyHandler = (source: FormControl | FormGroupDirective | NgForm | null) => boolean;

export const defaultStrategy: ValidationStrategy = (): StrategyHandler => {
  return (control, form) => {
    return (control && control.invalid && ((control.dirty && control.touched) || (form && form.submitted)));
  };
};

export const defaultServerStrategy: ServerValidationStrategy = (): ServerStrategyHandler => {
  return (source: FormControl | FormGroupDirective | null) => {
    return true;
  };
};

export const validationStrategy = new InjectionToken<StrategyHandler>('validationStrategy');
export const serverValidationStrategy = new InjectionToken<ServerStrategyHandler>('serverValidationStrategy');
