import {
  AfterViewInit,
  ChangeDetectorRef,
  Directive,
  HostBinding,
  Injector,
  Input,
  OnDestroy,
  Optional,
  ViewContainerRef
} from '@angular/core';
import { get, has } from 'lodash';
import { InputContainerComponent } from '../input-container/input-container.component';
import { takeUntil } from 'rxjs/operators';
import { FormControl, FormGroupDirective } from '@angular/forms';
import { ServerStrategyHandler, serverValidationStrategy } from './errors.strategies';
import { Subject } from 'rxjs/Subject';

//tslint:disable

@Directive({
  selector: 'app-server-error'
})
export class AppServerErrorDirective implements AfterViewInit, OnDestroy {
  @Input() public key: string[];
  @HostBinding('hidden') public hidden: boolean = true;

  @Input()
  public set validationStrategy(strategy) {
    this.strategy = strategy;
  }

  @Input()
  public set error(errors) {
    if (errors && has(errors, this.key.join('.'))) {
      this.errorMessage = get(errors, this.key.join('.'));
      this.viewContainer.element.nativeElement.innerHTML = this.errorMessage;
      if (this.control instanceof FormControl) {
        this.control.setErrors({serverError: this.errorMessage});
      }
      this.hidden = false;
    }
  };

  private control: FormControl | FormGroupDirective;
  private errorMessage: string;
  private until = new Subject();
  private strategy: ServerStrategyHandler;

  constructor(@Optional() private container: InputContainerComponent,
              @Optional() public form: FormGroupDirective,
              private injector: Injector,
              private viewContainer: ViewContainerRef,
              private cd: ChangeDetectorRef) {
  }

  public ngAfterViewInit() {
    if (!this.strategy) {
      this.strategy = this.injector.get(serverValidationStrategy);
    }
    this.control = this.container ? this.container.control : this.form;
    this.control.statusChanges
      .pipe(
        takeUntil(this.until),
      )
      .subscribe(() => {
        this.hidden = this.strategy(this.control);
        this.cd.markForCheck();
      });
  }

  public ngOnDestroy() {
    this.until.next();
    this.until.unsubscribe();
  }
}
