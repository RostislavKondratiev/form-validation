import {
  Directive,
  DoCheck,
  ElementRef, Host,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Optional,
  Self
} from '@angular/core';
import { FormFieldControl } from '../form-field-control';
import { Subject } from 'rxjs/Subject';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { CheckboxComponent } from './checkbox.component';

let nextUniqueId = 0;

@Directive({
  selector: 'app-checkbox[appInput]',
  providers: [{provide: FormFieldControl, useExisting: AppCheckboxDirective}]
})
export class AppCheckboxDirective implements FormFieldControl, DoCheck, OnDestroy {
  public readonly stateChange$: Subject<void> = new Subject<void>();
  public placeholder = '';
  public focused = false;

  protected _id: string;
  protected _uid = `app-checkbox-${nextUniqueId++}`;
  protected _disabled = false;
  protected _required = false;
  protected _readonly = false;

  @HostBinding('class.has-error')
  public hasError = false;

  @Input()
  public get id(): string {
    return this._id || this._uid;
  }

  public set id(value: string) {
    this._id = value || this._uid;
  }

  @Input()
  get disabled() {
    if (this.control && this.control.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }

  set disabled(value: any) {
    this._disabled = !!value;
    this.checkbox.setDisabledState(!!value);
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }

  public set required(value: boolean) {
    this._required = !!value;
  }

  public get value() {
    return this.control && this.control.control ? this.control.control.value : null;
  }

  public set value(value: string) {
    if (value !== this.value) {
      this.control.control.setValue(value);
      this.stateChange$.next();
    }
  }

  public get element() {
    return this.elem.nativeElement;
  }

  public get control() {
    return this.ngControl;
  }

  public get parent() {
    return this._parentFormGroup || this._parentForm;
  }

  constructor(@Host() private checkbox: CheckboxComponent,
              protected elem: ElementRef,
              @Optional() @Self() protected ngControl: NgControl,
              @Optional() protected _parentForm: NgForm,
              @Optional() protected _parentFormGroup: FormGroupDirective) {
    this.id = this.id;
  }

  @HostListener('blur', ['false'])
  @HostListener('focus', ['true'])
  public focusChanged(isFocused: boolean) {
    if (isFocused !== this.focused && !this._readonly) {
      this.focused = isFocused;
      this.stateChange$.next();
    }
  }

  public focus(): void {
    this.element.nativeElement.focus();
  }

  public ngDoCheck() {
    this.stateChange$.next();
  }

  public ngOnDestroy() {
    this.stateChange$.complete();
  }
}

