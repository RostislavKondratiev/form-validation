import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export const CHECKBOX_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CheckboxComponent), // tslint:disable-line
  multi: true,
};

let nextUniqueId = 0;

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [CHECKBOX_VALUE_ACCESSOR]
})
export class CheckboxComponent implements ControlValueAccessor {
  @Input() public checkboxSvg = '/assets/svg/checkbox-marked.svg';
  public value: boolean;
  public onChange: any;
  public onTouched: any;
  public disabled: boolean;
  public identifier = `app-checkbox-${nextUniqueId++}`;

  constructor(private cd: ChangeDetectorRef) {
  }

  public writeValue(obj: any): void {
    this.value = !!obj;
    this.cd.markForCheck();
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public switchCheckbox(val) {
    this.value = val;
    this.onChange(val);
    this.onTouched();
  }

  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.cd.markForCheck();
  }
}
