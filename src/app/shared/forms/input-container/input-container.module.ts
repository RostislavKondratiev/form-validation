import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppErrorsModule } from '../errors/errors.module';
import { InputContainerComponent } from './input-container.component';
import { AppInputDirective } from '../input/input.directive';
import { AppLabelDirective } from './label.directive';
import { AppHintDirective } from './hint.directive';
import { AppPlaceholderDirective } from './placeholder.directive';
import { AppAffixDirective } from './affix.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  defaultServerStrategy,
  defaultStrategy, ServerValidationStrategy,
  serverValidationStrategy, ValidationStrategy,
  validationStrategy
} from '../errors/errors.strategies';

const PROVIDERS = [
  {provide: validationStrategy, useFactory: defaultStrategy},
  {provide: serverValidationStrategy, useFactory: defaultServerStrategy},
];

const COMPONENTS = [
  InputContainerComponent,
];

const DIRECTIVES = [
  AppLabelDirective,
  AppHintDirective,
  AppAffixDirective,
  AppPlaceholderDirective,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AppErrorsModule,
  ],
  declarations: [
    ...DIRECTIVES,
    ...COMPONENTS,
  ],
  providers: [
    ...PROVIDERS
  ],
  exports: [
    ...DIRECTIVES,
    ...COMPONENTS,
  ]
})
export class AppInputContainerModule {
  public static setStrategies(strategy: ValidationStrategy, serverStrategy: ServerValidationStrategy = null): ModuleWithProviders {
    return {
      ngModule: AppInputContainerModule,
      providers: [
        {provide: validationStrategy, useFactory: strategy || defaultStrategy},
        {provide: serverValidationStrategy, useFactory: serverStrategy || defaultServerStrategy}
      ]
    };
  }
}
