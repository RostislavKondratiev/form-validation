import { Directive, HostBinding, Input } from '@angular/core';

//tslint:disable

@Directive({
  selector: 'app-hint'
})
export class AppHintDirective {
  @Input() public position: 'center' | 'left' | 'right' = 'right';

  @HostBinding('hidden')
  public hidden = false;

  @HostBinding('style.margin')
  public get align() {
    switch (this.position) {
      case 'center': {
        return '0 auto'
      }
      case 'left' : {
        return '0 auto 0 0'
      }
      case 'right' : {
        return '0 0 0 auto'
      }
      default: {
        return 'none'
      }
    }
  }
}
