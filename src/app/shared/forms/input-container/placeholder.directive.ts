import { Directive, HostBinding } from '@angular/core';

//tslint:disable

@Directive({
  selector: 'app-placeholder'
})
export class AppPlaceholderDirective {
  @HostBinding('hidden')
  public hidden = false;

  @HostBinding('style.left.px')
  public left = null;

}
