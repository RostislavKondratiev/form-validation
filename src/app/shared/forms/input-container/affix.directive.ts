import { Directive, ElementRef, HostBinding, Input } from '@angular/core';
import { AffixPositions } from './affix-positions';

//tslint:disable

@Directive({
  selector: 'app-affix'
})
export class AppAffixDirective {
  @Input() public type: 'suffix' | 'prefix' = 'prefix';
  @Input() public fontSize = 16;
  @Input() public transparent = false;

  @HostBinding('class.app-affix-background')
  public get affixBackground() {
    return !this.transparent;
  }

  @HostBinding('class.app-affix-prefix')
  public get positionRight() {
    return !this.transparent && this.type === AffixPositions.Prefix;
  }

  @HostBinding('class.app-affix-suffix')
  public get positionLeft() {
    return !this.transparent && this.type === AffixPositions.Suffix;
  }

  @HostBinding('class.app-affix-prefix-transparent')
  public get positionRightTransparent() {
    return this.transparent && this.type === AffixPositions.Prefix;
  }

  @HostBinding('class.app-affix-suffix-transparent')
  public get positionLeftTransparent() {
    return this.transparent && this.type === AffixPositions.Suffix;
  }

  @HostBinding('style.font-size.px')
  public get font() {
    return this.fontSize;
  }

  constructor(public elem: ElementRef) {
  }
}
