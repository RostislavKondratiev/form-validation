import { Directive, HostBinding, Input } from '@angular/core';

//tslint:disable

@Directive({
  selector: 'app-label',
})
export class AppLabelDirective {
  @Input() public position: 'left' | 'right' = 'left';

  @HostBinding('class.label-right')
  private get labelPosition() {
    return this.position === 'right';
  }
}
