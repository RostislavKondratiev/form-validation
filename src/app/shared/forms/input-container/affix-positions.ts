export enum AffixPositions {
  Suffix = 'suffix',
  Prefix = 'prefix'
}
