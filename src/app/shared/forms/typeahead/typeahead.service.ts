import { Injectable } from '@angular/core';

export interface SearchConfig {
  key: string;
  func: SearchFunction;
}

export type SearchFunction = (value: string) => any;

@Injectable()
export class TypeaheadService {
  private _searchConfig = [];

  public addSearch(data: SearchConfig): void {
    this._searchConfig.push(data);
  }

  public getSearchFunction(key: string): SearchFunction {
    const config = this._searchConfig.find((item) => item.key === key);
    return config ? config.func : null;
  }
}
