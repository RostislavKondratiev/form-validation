import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  forwardRef, Host, HostBinding,
  HostListener, Inject, Injector,
  Input,
  OnDestroy,
  Optional,
  QueryList,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { OptionModel } from '../option/option.model';
import { flyInOut } from '../../animations/fly-in-out';
import { OPTION_PARENT_COMPONENT, OptionComponent } from '../option/option/option.component';
import { debounceTime, takeUntil, tap } from 'rxjs/operators';
import { OptionsGroupComponent } from '../option/options-group/options-group.component';
import { InputContainerComponent } from '../input-container/input-container.component';
import { FormFieldControl } from '../form-field-control';
import { AppDropdownDirective } from './dropdown.directive';
import { InputPositions } from '../input-positions';

export const DROPDOWN_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DropdownComponent), // tslint:disable-line
  multi: true,
};

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    DROPDOWN_VALUE_ACCESSOR,
    {provide: OPTION_PARENT_COMPONENT, useExisting: DropdownComponent}
  ],
  animations: [flyInOut],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownComponent implements ControlValueAccessor, AfterViewInit, OnDestroy {
  @Input() public placeholder = 'Select Option';
  @Input() public noneField: OptionModel = {label: 'None', value: null};
  @Input() public searchable = false;
  @ContentChildren(OptionComponent, {descendants: true}) dropdownOptions: QueryList<OptionComponent>;
  @ContentChildren(OptionsGroupComponent, {descendants: true}) dropdownGroups: QueryList<OptionsGroupComponent>;
  @ViewChild(OptionComponent) empty: OptionComponent;

  public dropDownSearch = new FormControl();
  public showDropdown = false;
  public onChange: any;
  public onTouched: any;
  public searchValue: string;
  public notFound = false;
  public customPlaceholder = false;
  public directive: FormFieldControl;

  private _value: any;
  private until: Subject<void> = new Subject<void>();
  private disabled = false;

  public get inputPaddingLeft() {
    if (this.directive && this.directive.inputPosition.indexOf(InputPositions.LeftTransparent) !== -1) {
      return this.directive.inputIndent;
    } else {
      return null;
    }
  }

  public get inputPaddingRight() {
    if (this.directive && this.directive.inputPosition.indexOf(InputPositions.RightTransparent) !== -1) {
      return this.directive.inputIndent;
    } else {
      return null;
    }
  }

  public get value() {
    return this._value;
  }

  public set value(data: OptionModel | any) {
    if (typeof data === 'object') {
      this._value = data.value;
      this.placeholder = data.label;
    } else {
      this._value = data;
    }
    if (this.empty && data.value === this.empty.value) {
      this.placeholder = this.empty.label;
    } else if (this.dropdownOptions) {
      this.placeholder = (this.dropdownOptions.find((option: OptionComponent) => option.value === this._value)).label;
      if (this.searchable) {
        this.dropDownSearch.setValue(null, {emitEvent: false});
        this.resetSearch();
      }
    }
    this.showDropdown = false;
    this.onChange(this.value);
    this.onTouched();
  }

  constructor(private cd: ChangeDetectorRef,
              private element: ElementRef,
              @Optional() private container: InputContainerComponent,
              private injector: Injector) {
  }

  @HostListener('document:click', ['$event', '$event.target'])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
    this.cd.markForCheck();
    if (!targetElement || this.showDropdown !== true) {
      return;
    }

    const clickedInside = this.element.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.showDropdown = false;
      this.cd.markForCheck();
    }
  }

  public writeValue(obj: any): void {
    this._value = obj;
    if (this._value && this.dropdownOptions && this.dropdownOptions.length) {
      this.placeholder = (this.dropdownOptions.find((option: OptionComponent) => option.value === this._value)).label;
    }
    this.cd.detectChanges();
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public ngAfterViewInit() {
    this.directive = this.injector.get(FormFieldControl, null);
    if (this.container) {
      this.customPlaceholder = !!this.container.placeholder;
    }
    this.dropdownOptions.changes
      .pipe(takeUntil(this.until))
      .subscribe((items: QueryList<OptionComponent>) => {
        this.placeholder = (items.find((item: OptionComponent) => item.value === this._value)).label;
        this.cd.markForCheck();
      });

    this.dropDownSearch.valueChanges
      .pipe(
        takeUntil(this.until),
        debounceTime(300),
        tap((v) => this.searchValue = v)
      )
      .subscribe((value: string) => {
        this.dropdownOptions.forEach((option: OptionComponent) => {
          option.hidden = !option.label.toLowerCase().match(value.toLowerCase());
        });
        if (this.dropdownGroups.length > 0) {
          this.filterGroups(this.dropdownGroups, value);
        }
        this.notFound = !this.dropdownOptions.find((option) => !option.hidden);
        this.cd.markForCheck();
      });

    const label = this.dropdownOptions.find((option: OptionComponent) => option.value === this.value);
    if (label) {
      this.placeholder = label.label;
    }
    this.cd.detectChanges();
  }

  public toggleDropdown() {
    if (!this.disabled) {
      this.showDropdown = !this.showDropdown;
      this.cd.detectChanges();
    }
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public ngOnDestroy() {
    this.until.next();
    this.until.complete();
  }

  private resetSearch() {
    this.dropdownOptions.forEach((option: OptionComponent) => option.hidden = false);
    this.cd.markForCheck();
  }

  private filterGroups(groups: QueryList<OptionsGroupComponent>, value: string) {
    const foundedGroups = [];
    groups.forEach((group: OptionsGroupComponent) => {
      group.hidden = false;
      if (!group.label.toLocaleLowerCase().match(value.toLocaleLowerCase())) {
        group.options.forEach((option: OptionComponent) => {
          option.hidden = !option.label.toLocaleLowerCase().match(value.toLocaleLowerCase());
        });
      } else {
        foundedGroups.push(group);
      }
    });
    foundedGroups.forEach((group: OptionsGroupComponent) => {
      group.options.forEach((option) => option.hidden = false);
    });
  }
}
