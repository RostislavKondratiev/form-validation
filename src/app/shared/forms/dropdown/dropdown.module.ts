import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownComponent } from './dropdown.component';
import { AppDropdownDirective } from './dropdown.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppOptionsModule } from '../option/options.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AppOptionsModule,
  ],
  declarations: [
    DropdownComponent,
    AppDropdownDirective
  ],
  exports: [
    DropdownComponent,
    AppDropdownDirective
  ]
})
export class AppDropdownModule {

}
