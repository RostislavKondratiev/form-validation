import {
  Directive,
  DoCheck,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Optional,
  Self
} from '@angular/core';
import { FormFieldControl } from '../form-field-control';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { DropdownComponent } from './dropdown.component';
import { InputPositions } from '../input-positions';

let nextUniqueId = 0;

@Directive({
  selector: 'app-dropdown[appInput]',
  providers: [{provide: FormFieldControl, useExisting: AppDropdownDirective}]
})
export class AppDropdownDirective implements FormFieldControl, DoCheck, OnDestroy {
  public readonly stateChange$: Subject<void> = new Subject<void>();
  public inputPosition: Array<'left' | 'right' | 'left-transparent' | 'right-transparent'> = [];
  public focused = false;

  protected _inputIndent;
  protected _id: string;
  protected _uid = `app-dropdown-${nextUniqueId++}`;
  protected _disabled = false;
  protected _required = false;
  protected _placeholder = '';
  protected _readonly = false;

  @HostBinding('attr.tabindex')
  public tabIndex = -1;

  @HostBinding('class.has-error')
  public hasError = false;

  @HostBinding('class.input-right')
  public get inputRight() {
    return this.inputPosition.indexOf(InputPositions.Right) !== -1;
  }

  @HostBinding('class.input-left')
  public get inputLeft() {
    return this.inputPosition.indexOf(InputPositions.Left) !== -1;
  }

  @HostBinding('class.input-right-transparent')
  public get inputRightTransparent() {
    return this.inputPosition.indexOf(InputPositions.RightTransparent) !== -1;
  }

  @HostBinding('class.input-left-transparent')
  public get inputLeftTransparent() {
    return this.inputPosition.indexOf(InputPositions.LeftTransparent) !== -1;
  }

  @HostBinding('attr.id')
  @Input()
  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value || this._uid;
  }

  @Input()
  get disabled() {
    if (this.control && this.control.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }

  set disabled(value: any) {
    this._disabled = !!value;
    this.dropdown.setDisabledState(!!value);
  }

  @Input()
  get placeholder() {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }

  public set required(value: boolean) {
    this._required = !!value;
  }

  @Input()
  public get readonly(): boolean {
    return this._readonly;
  }

  public set readonly(value: boolean) {
    this._readonly = !!value;
  }

  public get inputIndent() {
    return this._inputIndent;
  }

  public set inputIndent(indent: number) {
    this._inputIndent = indent;
  }

  public get value() {
    return this.control && this.control.control ? this.control.control.value : null;
  }

  public set value(value: string) {
    if (value !== this.value) {
      this.control.control.setValue(value);
      this.stateChange$.next();
    }
  }

  public get element() {
    return this.elem.nativeElement;
  }

  public get control() {
    return this.ngControl;
  }

  public get parent() {
    return this._parentFormGroup || this._parentForm;
  }

  constructor(private dropdown: DropdownComponent,
              protected elem: ElementRef,
              @Optional() @Self() protected ngControl: NgControl,
              @Optional() protected _parentForm: NgForm,
              @Optional() protected _parentFormGroup: FormGroupDirective) {
    this.id = this.id;
  }

  @HostListener('blur', ['false'])
  @HostListener('focus', ['true'])
  public focusChanged(isFocused: boolean) {
    if (isFocused !== this.focused && !this._readonly) {
      this.focused = isFocused;
      this.stateChange$.next();
    }
  }

  public focus(): void {
    this.element.nativeElement.focus();
  }

  public ngDoCheck() {
    this.stateChange$.next();
  }

  public ngOnDestroy() {
    this.stateChange$.complete();
  }

}
