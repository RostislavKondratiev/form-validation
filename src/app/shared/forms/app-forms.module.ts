import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  defaultServerStrategy,
  defaultStrategy,
  ServerValidationStrategy,
  serverValidationStrategy,
  ValidationStrategy,
  validationStrategy
} from './errors/errors.strategies';
import { AppRadioModule } from './radio/radio.module';
import { AppRadioStandaloneModule } from './radio-standalone/radio-standalone.module';
import { AppTypeaheadModule } from './typeahead/typeahead.module';
import { AppCheckboxModule } from './checkbox/checkbox.module';
import { AppInputContainerModule } from './input-container/input-container.module';
import { AppDropdownModule } from './dropdown/dropdown.module';
import { AppOptionsModule } from './option/options.module';
import { AppErrorsModule } from './errors/errors.module';
import { AppInputModule } from './input/input.module';

const MODULES = [
  FormsModule,
  ReactiveFormsModule,
  AppInputModule,
  AppErrorsModule,
  AppOptionsModule,
  AppTypeaheadModule,
  AppCheckboxModule,
  AppInputContainerModule,
  AppDropdownModule,
  AppRadioModule,
  AppRadioStandaloneModule,
];

@NgModule({
  imports: [
    CommonModule,
    ...MODULES
  ],
  exports: [
    ...MODULES
  ]
})
export class AppFormsModule {
  public static setStrategies(strategy: ValidationStrategy, serverStrategy: ServerValidationStrategy = null): ModuleWithProviders {
    return {
      ngModule: AppFormsModule,
      providers: [
        {provide: validationStrategy, useFactory: strategy || defaultStrategy},
        {provide: serverValidationStrategy, useFactory: serverStrategy || defaultServerStrategy}
      ]
    };
  }
}
