import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioGroupComponent } from './radio-group/radio-group.component';
import { RadioItemComponent } from './radio-item/radio-item.component';
import { RadioGroupDirective } from './radio-group/radio-group.directive';

const DIRECTIVES = [
  RadioGroupDirective,
];

const COMPONENTS = [
  RadioGroupComponent,
  RadioItemComponent,
];

@NgModule({
  imports: [CommonModule],
  declarations: [
    ...DIRECTIVES,
    ...COMPONENTS
  ],
  exports: [
    ...DIRECTIVES,
    ...COMPONENTS
  ]
})
export class AppRadioModule {

}
