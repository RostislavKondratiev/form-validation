import {
  Directive,
  DoCheck,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Optional,
  Self
} from '@angular/core';
import { FormFieldControl } from '../../form-field-control';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { RadioGroupComponent } from './radio-group.component';
import { Subject } from 'rxjs/Subject';

let nextUniqueId = 0;

@Directive({
  selector: 'app-radio-group[appInput]',
  providers: [{provide: FormFieldControl, useExisting: RadioGroupDirective}]
})
export class RadioGroupDirective implements FormFieldControl, DoCheck, OnDestroy {
  public readonly stateChange$: Subject<void> = new Subject<void>();
  public inputPosition: Array<'left' | 'right' | 'left-transparent' | 'right-transparent'> = [];
  public hasError = false;

  public focused = false;
  protected _id: string;
  protected _uid = `app-radio-group-${nextUniqueId++}`;
  protected _disabled = false;
  protected _required = false;
  protected _placeholder = '';
  protected _readonly = false;
  protected _inputIndent: number;

  @HostBinding('attr.id')
  @Input()
  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value || this._uid;
  }

  @Input()
  public get disabled() {
    if (this.control && this.control.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }

  public set disabled(value: any) {
    this._disabled = !!value;
    this.radioGroup.setDisabledState(!!value);
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }

  public set required(value: boolean) {
    this._required = !!value;
  }

  public get inputIndent() {
    return this._inputIndent;
  }

  public set inputIndent(indent: number) {
    this._inputIndent = indent;
  }

  public get value() {
    return this.control && this.control.control ? this.control.control.value : null;
  }

  public set value(value: string) {
    if (value !== this.value) {
      this.control.control.setValue(value);
      this.stateChange$.next();
    }
  }

  constructor(private radioGroup: RadioGroupComponent,
              protected elem: ElementRef,
              @Optional() @Self() protected ngControl: NgControl,
              @Optional() protected _parentForm: NgForm,
              @Optional() protected _parentFormGroup: FormGroupDirective) {
    this.id = this.id;
  }

  @HostListener('blur', ['false'])
  @HostListener('focus', ['true'])
  public focusChanged(isFocused: boolean) {
    if (isFocused !== this.focused && !this._readonly) {
      this.focused = isFocused;
      this.stateChange$.next();
    }
  }

  public get element() {
    return this.elem.nativeElement;
  }

  public get control() {
    return this.ngControl;
  }

  public get parent() {
    return this._parentFormGroup || this._parentForm;
  }

  public focus(): void {
    this.element.nativeElement.focus();
  }

  public ngDoCheck() {
    this.stateChange$.next();
  }

  public ngOnDestroy() {
    this.stateChange$.complete();
  }
}
