export abstract class OptionItem {
  public hidden: boolean;
  public label: string;
}
