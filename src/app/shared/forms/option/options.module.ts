import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptionsGroupComponent } from './options-group/options-group.component';
import { OptionComponent } from './option/option.component';

const COMPONENTS = [
  OptionComponent,
  OptionsGroupComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ...COMPONENTS,
  ],
  exports: [
    ...COMPONENTS
  ]
})
export class AppOptionsModule {

}
