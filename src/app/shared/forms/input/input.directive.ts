import {
  ChangeDetectorRef,
  Directive,
  DoCheck,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnDestroy,
  Optional,
  Self
} from '@angular/core';
import { FormFieldControl } from '../form-field-control';
import { Subject } from 'rxjs/Subject';
import { FormGroupDirective, NgControl, NgForm } from '@angular/forms';
import { InputPositions } from '../input-positions';

let nextUniqueId = 0;

@Directive({
  selector: 'input[appInput], textarea[appInput]',
  providers: [{provide: FormFieldControl, useExisting: AppInputDirective}],
})
export class AppInputDirective implements FormFieldControl, DoCheck, OnDestroy {
  public readonly stateChange$: Subject<void> = new Subject<void>();
  public inputPosition: Array<'left' | 'right' | 'left-transparent' | 'right-transparent'> = [];
  public inputIndent: number;

  protected _id: string;
  protected _uid = `app-input-${nextUniqueId++}`;

  @HostBinding('class.form-control')
  public controlClass = true;

  @HostBinding('class.focused')
  public focused = false;

  @HostBinding('class.has-error')
  public hasError = false;

  @HostBinding('disabled')
  protected _disabled = false;

  @HostBinding('required')
  protected _required = false;

  @HostBinding('placeholder')
  protected _placeholder = '';

  @HostBinding('readonly')
  protected _readonly = false;

  @Input()
  get disabled() {
    if (this.control && this.control.disabled !== null) {
      return this.ngControl.disabled;
    }
    return this._disabled;
  }

  set disabled(value: any) {
    this._disabled = !!value;
    if (this.focused) {
      this.focused = false;
      this.stateChange$.next();
    }
  }

  @Input()
  get placeholder() {
    return this._placeholder;
  }

  set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input()
  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value || this._uid;
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }

  public set required(value: boolean) {
    this._required = !!value;
  }

  @Input()
  public get readonly(): boolean {
    return this._readonly;
  }

  public set readonly(value: boolean) {
    this._readonly = !!value;
  }

  @HostBinding('class.input-right')
  public get inputRight() {
    return this.inputPosition.indexOf(InputPositions.Right) !== -1;
  }

  @HostBinding('class.input-left')
  public get inputLeft() {
    return this.inputPosition.indexOf(InputPositions.Left) !== -1;
  }

  @HostBinding('style.padding-left.px')
  public get inputPaddingLeft() {
    if (this.inputPosition.indexOf(InputPositions.LeftTransparent) !== -1) {
      return this.inputIndent;
    } else {
      return null;
    }
  }

  @HostBinding('style.padding-right.px')
  public get inputPaddingRight() {
    if (this.inputPosition.indexOf(InputPositions.RightTransparent) !== -1) {
      return this.inputIndent;
    } else {
      return null;
    }
  }

  public get value() {
    return this.element.nativeElement.value;
  }

  public set value(value: string) {
    if (value !== this.value) {
      this.element.nativeElement.value = value;
      this.stateChange$.next();
    }
  }

  public get element() {
    return this.elem.nativeElement;
  }

  public get control() {
    return this.ngControl;
  }

  public get parent() {
    return this._parentFormGroup || this._parentForm;
  }

  constructor(protected elem: ElementRef,
              @Optional() @Self() protected ngControl: NgControl,
              @Optional() protected _parentForm: NgForm,
              @Optional() protected _parentFormGroup: FormGroupDirective,
              private cd: ChangeDetectorRef) {
    this.id = this.id;
  }

  @HostListener('blur', ['false'])
  @HostListener('focus', ['true'])
  public focusChanged(isFocused: boolean) {
    if (isFocused !== this.focused && !this._readonly) {
      this.focused = isFocused;
      this.stateChange$.next();
    }
  }

  public focus(): void {
    this.element.nativeElement.focus();
  }

  public ngDoCheck() {
    this.stateChange$.next();
  }

  public ngOnDestroy() {
    this.stateChange$.complete();
  }

}
