import { Directive, HostBinding, HostListener, Input, OnInit } from '@angular/core';
import { RadioGroupStandaloneDirective } from './radio-group-standalone.directive';

@Directive({
  selector: '[appRadio]'
})
export class RadioStandaloneDirective implements OnInit {
  @Input() public value: any;

  @HostBinding('class.disabled')
  @Input() public disabled = false;

  @HostBinding('class.standalone-radio-checked')
  public checked = false;

  @HostListener('click')
  public onClick() {
    if (!this.disabled) {
      if (this.group.pressOut && this.group.value === this.value) {
        this.group.value = null;
        return;
      } else if (this.group.value === this.value) {
        return;
      }
      this.group.value = this.value;
    }
  }

  constructor(private group: RadioGroupStandaloneDirective) {
  }

  public ngOnInit() {
    this.group.register(this);
  }
}
