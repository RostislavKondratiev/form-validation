import { Directive, forwardRef, HostBinding, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RadioStandaloneDirective } from './radio-standalone.directive';

const APP_RADIO_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RadioGroupStandaloneDirective),
  multi: true
};

let nextUniqueId = 0;

@Directive({
  selector: '[appRadioGroup]',
  providers: [APP_RADIO_VALUE_ACCESSOR]
})
export class RadioGroupStandaloneDirective implements ControlValueAccessor {
  private radios: RadioStandaloneDirective[] = [];
  private _value = null;
  private _disabled: boolean;
  protected _uid = `app-radio-standalone-group-${nextUniqueId++}`;
  protected _id;

  @HostBinding('attr.id')
  @Input()
  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value || this._uid;
  }

  @Input() public pressOut = false;
  public onChange: any;
  public onTouched: any;

  public get value() {
    return this._value;
  }

  public set value(val) {
    this._value = val;
    this.onChange(val);
    this.updateRadiosValue();
  }

  @Input()
  public get disabled() {
    return this._disabled;
  }

  public set disabled(isDisabled: boolean) {
    this.setDisabledState(isDisabled);
  }

  constructor() {
    this.id = this.id;
  }

  public registerOnChange(fn: (value: any) => any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => any): void {
    this.onTouched = fn;
  }

  public writeValue(value) {
    this._value = value;
    this.updateRadiosValue();
  }

  public setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
    this.updateRadiosDisabled();
  }

  public register(radio: RadioStandaloneDirective) {
    this.radios.push(radio);
  }

  private updateRadiosValue() {
    this.radios.forEach((radio) => {
      radio.checked = radio.value === this.value;
    });
  }

  private updateRadiosDisabled() {
    this.radios.forEach((radio) => {
      radio.disabled = true;
    });
  }
}
