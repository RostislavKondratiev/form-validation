import { animate, state, style, transition, trigger } from '@angular/animations';

export const flyInOut = trigger('flyInOut', [
  state('in', style({transform: 'translateY(10%)', opacity: 0})),
  transition('void => *', [
    style({transform: 'translateY(-10%)', opacity: 1}),
    animate(100)
  ]),
  transition('* => void', [
    animate(100, style({transform: 'translateY(-10%)', opacity: 0}))
  ])
]);
