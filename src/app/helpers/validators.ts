import { AbstractControl, ValidationErrors } from '@angular/forms';

type ValidationResult =
  ValidationErrors
  | null;

export class AppValidators {
  public static email(control: AbstractControl): ValidationResult {
    const EMAIL_REGEXP = /^([\w+\-]\.?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]{2,}$/i;
    const errors: { [key: string]: boolean } = {};
    if (control.value && control.value !== '') {
      if (!EMAIL_REGEXP.test(control.value)) {
        errors.email = true;
      }
      if (control.value.length >= 100) {
        errors.maxlength = true;
      }
    }
    return errors;
  }
}
